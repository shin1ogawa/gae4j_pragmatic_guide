package gaej2011;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.Future;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.AppEngineTestCase;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.apphosting.api.ApiProxy;
import com.google.apphosting.api.ApiProxy.ApiConfig;
import com.google.apphosting.api.ApiProxy.ApiProxyException;
import com.google.apphosting.api.ApiProxy.CapabilityDisabledException;
import com.google.apphosting.api.ApiProxy.Delegate;
import com.google.apphosting.api.ApiProxy.Environment;
import com.google.apphosting.api.ApiProxy.LogRecord;

public class ReadOnlyDatastoreTest extends AppEngineTestCase {

    Key doWriteOperation1() {
        return Datastore.put(new Entity("test"));
    }

    Key doWriteOperation2() {
        try {
            return Datastore.put(new Entity("test"));
        } catch (CapabilityDisabledException e) {
            return null;
        }
    }

    @Test(expected = CapabilityDisabledException.class)
    public void writeOp1() {
        doWriteOperation1();
    }

    @Test
    public void writeOp2() {
        assertThat(doWriteOperation2(), is(nullValue()));
    }

    ReadOnlyDelegate delegate;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        delegate = new ReadOnlyDelegate();
        ApiProxy.setDelegate(delegate);
    }

    @Override
    public void tearDown() throws Exception {
        ApiProxy.setDelegate(delegate.parent);
        super.tearDown();
    }

    static class ReadOnlyDelegate implements Delegate<Environment> {
        @SuppressWarnings("unchecked")
        final Delegate<Environment> parent = ApiProxy.getDelegate();

        public void flushLogs(Environment environment) {
            parent.flushLogs(environment);
        }

        public List<Thread> getRequestThreads(Environment environment) {
            return parent.getRequestThreads(environment);
        }

        public void log(Environment environment, LogRecord record) {
            parent.log(environment, record);
        }

        public Future<byte[]> makeAsyncCall(Environment environment,
                String packageName, String methodName, byte[] request,
                ApiConfig apiConfig) {
            emulateCapabilityDisabledException(
                environment,
                packageName,
                methodName);
            return parent.makeAsyncCall(
                environment,
                packageName,
                methodName,
                request,
                apiConfig);
        }

        public byte[] makeSyncCall(Environment environment, String packageName,
                String methodName, byte[] request) throws ApiProxyException {
            emulateCapabilityDisabledException(
                environment,
                packageName,
                methodName);
            return parent.makeSyncCall(
                environment,
                packageName,
                methodName,
                request);
        }

        void emulateCapabilityDisabledException(Environment environment,
                String packageName, String methodName) {
            if (packageName.equals("datastore_v3") == false) {
                return;
            }
            if (methodName.equals("Put") || methodName.equals("Delete")) {
                throw new CapabilityDisabledException(
                    "ReadOnlyDelegate",
                    packageName,
                    methodName);
            }
        }
    }
}
