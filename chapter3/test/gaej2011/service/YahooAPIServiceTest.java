package gaej2011.service;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;
import org.slim3.tester.AppEngineTestCase;
import org.slim3.tester.URLFetchHandler;
import org.xml.sax.SAXException;

import com.google.appengine.api.urlfetch.URLFetchServicePb.URLFetchRequest;

public class YahooAPIServiceTest extends AppEngineTestCase {

    @Test
    public void 形態素解析APIで単語を抽出する() throws IOException, SAXException,
            ParserConfigurationException {
        tester.setUrlFetchHandler(new URLFetchHandler() {
            public int getStatusCode(URLFetchRequest paramURLFetchRequest)
                    throws IOException {
                return 200;
            }

            public byte[] getContent(URLFetchRequest paramURLFetchRequest)
                    throws IOException {
                File testXmlFile = new File("test/yahooapi.xml");
                byte[] contents = new byte[(int) testXmlFile.length()];
                DataInputStream input =
                    new DataInputStream(new FileInputStream(testXmlFile));
                input.readFully(contents);
                input.close();
                return contents;
            }
        });
        Set<String> parsed =
            gaej2011.service.YahooAPIService.parse(" 本日は晴天なり本日は晴天なり");
        assertThat(" 重複した単語は排除される", parsed.size(), is(2));
        Iterator<String> iterator = parsed.iterator();
        assertThat(iterator.next(), is(" 本日"));
        assertThat(iterator.next(), is(" 晴天"));
    }
}
