package gaej2011;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.Future;

import org.junit.Test;
import org.slim3.tester.AppEngineTestCase;

import com.google.appengine.api.oauth.OAuthRequestException;
import com.google.appengine.api.oauth.OAuthServiceFactory;
import com.google.appengine.api.users.User;
import com.google.apphosting.api.ApiProxy;
import com.google.apphosting.api.ApiProxy.ApiConfig;
import com.google.apphosting.api.ApiProxy.ApiProxyException;
import com.google.apphosting.api.ApiProxy.LogRecord;
import com.google.apphosting.api.UserServicePb;

public class OAuthTest extends AppEngineTestCase {

    User getCurrentUser() {
        try {
            return OAuthServiceFactory.getOAuthService().getCurrentUser();
        } catch (OAuthRequestException e) {
            return null;
        }
    }

    @Test
    public void ng() {
        User user = getCurrentUser();
        assertThat(user, is(nullValue()));
    }

    @Test
    public void ok() {
        String email = "uso800@gmail.com";
        UserServicePb.GetOAuthUserResponse mockUser =
            new UserServicePb.GetOAuthUserResponse();
        mockUser.setEmail(email);
        // tester.environment.setEmail(email);
        delegate.setCurrentUser(mockUser);
        User user = getCurrentUser();
        assertThat(user, is(notNullValue()));
        assertThat(user.getEmail(), is(email));
    }

    OAuthDelegate delegate;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        delegate = new OAuthDelegate();
        ApiProxy.setDelegate(delegate);
    }

    @Override
    public void tearDown() throws Exception {
        ApiProxy.setDelegate(delegate.parent);
        super.tearDown();
    }

    static class OAuthDelegate implements
            ApiProxy.Delegate<ApiProxy.Environment> {
        static final String ENV_KEY =
            "com.google.appengine.api.oauth.OAuthService.get_oauth_user_response";
        @SuppressWarnings("unchecked")
        final ApiProxy.Delegate<ApiProxy.Environment> parent = ApiProxy
            .getDelegate();
        private UserServicePb.GetOAuthUserResponse user = null;

        void setCurrentUser(UserServicePb.GetOAuthUserResponse user) {
            this.user = user;
            ApiProxy.getCurrentEnvironment().getAttributes().put(ENV_KEY, user);
        }

        void clearCurrentUser() {
            this.user = null;
            ApiProxy.getCurrentEnvironment().getAttributes().remove(ENV_KEY);
        }

        @Override
        public void flushLogs(ApiProxy.Environment env) {
            parent.flushLogs(env);
        }

        @Override
        public List<Thread> getRequestThreads(ApiProxy.Environment env) {
            return parent.getRequestThreads(env);
        }

        @Override
        public void log(ApiProxy.Environment env, LogRecord logRecord) {
            parent.log(env, logRecord);
        }

        @Override
        public Future<byte[]> makeAsyncCall(ApiProxy.Environment env,
                String service, String method, byte[] requestBytes,
                ApiConfig apiConfig) {
            return parent.makeAsyncCall(
                env,
                service,
                method,
                requestBytes,
                apiConfig);
        }

        @Override
        public byte[] makeSyncCall(ApiProxy.Environment env, String service,
                String method, byte[] requestBytes) throws ApiProxyException {
            if (service.equals("user") && method.equals("GetOAuthUser")) {
                if (user == null) {
                    throw new ApiProxy.ApplicationException(
                        UserServicePb.UserServiceError.ErrorCode.NOT_ALLOWED.ordinal(),
                        null);
                }
            }
            return parent.makeSyncCall(env, service, method, requestBytes);
        }
    }
}