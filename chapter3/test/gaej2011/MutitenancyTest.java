package gaej2011;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.AppEngineTestCase;

import com.google.appengine.api.NamespaceManager;
import com.google.appengine.api.datastore.Entity;

public class MutitenancyTest extends AppEngineTestCase {

    List<Entity> queryAll(String namespace) {
        String backup = NamespaceManager.get();
        NamespaceManager.set(namespace);
        try {
            return Datastore.query("TestEntities").asList();
        } finally {
            NamespaceManager.set(backup);
        }
    }

    @Test
    public void NamespaceManagerの動作確認() {
        NamespaceManager.set("company-A");
        for (int i = 0; i < 5; i++) {
            Datastore.put(new Entity("TestEntities"));
        }
        NamespaceManager.set("company-B");
        for (int i = 0; i < 10; i++) {
            Datastore.put(new Entity("TestEntities"));
        }
        assertThat(queryAll("company-A").size(), is(5));
        assertThat(queryAll("company-B").size(), is(10));
        assertThat(queryAll("company-C").size(), is(0));
    }
}
