package gaej2011.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import gaej2011.meta.LogDTOMeta;
import gaej2011.model.LogDTO;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.slim3.tester.ControllerTestCase;

import com.google.apphosting.api.ApiProxy;
import com.google.apphosting.api.ApiProxy.ApiConfig;
import com.google.apphosting.api.ApiProxy.ApiProxyException;
import com.google.apphosting.api.ApiProxy.LogRecord;
import com.google.apphosting.api.logservice.LogServicePb;
import com.google.apphosting.api.logservice.LogServicePb.LogLine;
import com.google.apphosting.api.logservice.LogServicePb.LogOffset;
import com.google.apphosting.api.logservice.LogServicePb.RequestLog;

public class LogControllerTest extends ControllerTestCase {

    static final String PATH = "/Log";

    @Test
    public void ログを取得する() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException, Exception {
        LogDelegate delegate = new LogDelegate();
        ApiProxy.setDelegate(delegate);
        tester.start(PATH);
        assertThat(
            "LogController のインスタンスが使用される",
            tester.getController(),
            instanceOf(LogController.class));
        assertThat(
            " レスポンスコードが200",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_OK));
        assertThat(
            "Content-Type はapplication/json",
            tester.response.getContentType(),
            is("application/json"));
        LogDTO[] logs =
            LogDTOMeta.get().jsonToModels(tester.response.getOutputAsString());
        assertThat(logs.length, is(20));
        ApiProxy.setDelegate(delegate.parentDelegate);
    }

    static class LogDelegate implements ApiProxy.Delegate<ApiProxy.Environment> {
        @SuppressWarnings("unchecked")
        final ApiProxy.Delegate<ApiProxy.Environment> parentDelegate = ApiProxy
            .getDelegate();

        @Override
        public void flushLogs(ApiProxy.Environment env) {
            parentDelegate.flushLogs(env);
        }

        @Override
        public List<Thread> getRequestThreads(ApiProxy.Environment env) {
            return parentDelegate.getRequestThreads(env);
        }

        @Override
        public void log(ApiProxy.Environment env, LogRecord logRecord) {
            parentDelegate.log(env, logRecord);
        }

        @Override
        public Future<byte[]> makeAsyncCall(ApiProxy.Environment env,
                String service, String method, byte[] requestBytes,
                ApiConfig apiConfig) {
            if (service.equals("logservice") == false
                || method.equals("Read") == false) {
                return parentDelegate.makeAsyncCall(
                    env,
                    service,
                    method,
                    requestBytes,
                    apiConfig);
            }
            LogServicePb.LogReadRequest requestPb =
                new LogServicePb.LogReadRequest();
            requestPb.mergeFrom(requestBytes);
            final LogServicePb.LogReadResponse responsePb =
                new LogServicePb.LogReadResponse();
            for (int i = 0; i < requestPb.getCount(); i++) {
                RequestLog log = new RequestLog();
                log
                    .setCombined("0.0.0.0 - loginUser [01/Dec/2011:00:00:00 -0000]"
                        + " \"GET /path HTTP/1.1\" 200 100 \"http://google.com/\" \"Chrome\"");
                log.setOffset(new LogOffset().setRequestId(String.valueOf(i)));
                log.addLine(new LogLine()
                    .setLogMessage("\npath\nException:\"\nmessage\"\n\\"));
                responsePb.addLog(log);
            }
            return new Future<byte[]>() {
                @Override
                public boolean cancel(boolean arg0) {
                    return false;
                }

                @Override
                public byte[] get() {
                    return responsePb.toByteArray();
                }

                @Override
                public byte[] get(long arg0, TimeUnit arg1) {
                    return get();
                }

                @Override
                public boolean isCancelled() {
                    return false;
                }

                @Override
                public boolean isDone() {
                    return true;
                }
            };
        }

        @Override
        public byte[] makeSyncCall(ApiProxy.Environment env, String service,
                String method, byte[] requestBytes) throws ApiProxyException {
            return parentDelegate.makeSyncCall(
                env,
                service,
                method,
                requestBytes);
        }
    }
}
