package gaej2011.controller.cron;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import gaej2011.model.AccessCounter;
import gaej2011.service.MinutesService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.Future;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.ControllerTestCase;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.taskqueue.TaskQueuePb;
import com.google.apphosting.api.ApiProxy;
import com.google.apphosting.api.ApiProxy.ApiConfig;
import com.google.apphosting.api.ApiProxy.ApiProxyException;
import com.google.apphosting.api.ApiProxy.Delegate;
import com.google.apphosting.api.ApiProxy.Environment;
import com.google.apphosting.api.ApiProxy.LogRecord;

public class AccessCountControllerTest extends ControllerTestCase {

    static final String PATH = "/cron/AccessCount";

    @Test
    public void アクセスカウンタを集計する() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        List<Key> minutesKeys = createTestTasks();
        int before = tester.count(AccessCounter.class);
        tester.start(PATH);
        assertThat(
            "AccessCountController のインスタンスが使用される",
            tester.getController(),
            instanceOf(AccessCountController.class));
        assertThat(
            " レスポンスコードが200",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_OK));
        int after = tester.count(AccessCounter.class);
        assertThat(" テスト用の議事録の数だけアクセスカウンタが記録される", after, is(before
            + minutesKeys.size()));
        List<AccessCounter> counters =
            Datastore.query(AccessCounter.class).asList();
        for (AccessCounter accessCounter : counters) {
            assertThat(
                " 全ての議事録につき2 回ずつアクセスがある",
                accessCounter.getCount(),
                is(2L));
        }
        assertThat(" タスクが全て削除される", delegate.tasks.size(), is(0));
    }

    private List<Key> createTestTasks() {
        List<Key> minutesKeys = new ArrayList<Key>();
        for (int i = 0; i < 5; i++) { // 5 件の議事録
            Key minutesKey = MinutesService.put(" 議事録" + i);
            minutesKeys.add(minutesKey);
            TaskQueuePb.TaskQueueQueryAndOwnTasksResponse.Task task1 =
                new TaskQueuePb.TaskQueueQueryAndOwnTasksResponse.Task();
            task1.setTaskName("task-first" + i);
            task1.setBody("minutesKey=" + Datastore.keyToString(minutesKey));
            delegate.tasks.add(task1); // 1 回目のアクセス
            TaskQueuePb.TaskQueueQueryAndOwnTasksResponse.Task task2 =
                new TaskQueuePb.TaskQueueQueryAndOwnTasksResponse.Task();
            task2.setTaskName("task-second" + i);
            task2.setBody("minutesKey=" + Datastore.keyToString(minutesKey));
            delegate.tasks.add(task2); // 2 回目のアクセス
        }
        return minutesKeys;
    }

    PullTasksDelegate delegate;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        delegate = new PullTasksDelegate();
        ApiProxy.setDelegate(delegate);
    }

    @Override
    public void tearDown() throws Exception {
        ApiProxy.setDelegate(delegate.parent);
        super.tearDown();
    }

    static class PullTasksDelegate implements Delegate<Environment> {
        @SuppressWarnings("unchecked")
        final Delegate<Environment> parent = ApiProxy.getDelegate();

        public void flushLogs(Environment environment) {
            parent.flushLogs(environment);
        }

        public List<Thread> getRequestThreads(Environment environment) {
            return parent.getRequestThreads(environment);
        }

        public void log(Environment environment, LogRecord record) {
            parent.log(environment, record);
        }

        public Future<byte[]> makeAsyncCall(Environment environment,
                String packageName, String methodName, byte[] request,
                ApiConfig apiConfig) {
            return parent.makeAsyncCall(
                environment,
                packageName,
                methodName,
                request,
                apiConfig);
        }

        List<TaskQueuePb.TaskQueueQueryAndOwnTasksResponse.Task> tasks =
            new ArrayList<TaskQueuePb.TaskQueueQueryAndOwnTasksResponse.Task>();

        public byte[] makeSyncCall(Environment environment, String packageName,
                String methodName, byte[] request) throws ApiProxyException {
            if (packageName.equals("taskqueue")
                && methodName.equals("QueryAndOwnTasks")) {
                TaskQueuePb.TaskQueueQueryAndOwnTasksResponse responsePb =
                    new TaskQueuePb.TaskQueueQueryAndOwnTasksResponse();
                for (TaskQueuePb.TaskQueueQueryAndOwnTasksResponse.Task task : tasks) {
                    responsePb.addTask(task);
                }
                return responsePb.toByteArray();
            } else if (packageName.equals("taskqueue")
                && methodName.equals("Delete")) {
                TaskQueuePb.TaskQueueDeleteRequest requestPb =
                    new TaskQueuePb.TaskQueueDeleteRequest();
                requestPb.mergeFrom(request);
                int count = requestPb.taskNameSize();
                for (int i = 0; i < count; i++) {
                    String taskName = requestPb.getTaskName(i);
                    ListIterator<TaskQueuePb.TaskQueueQueryAndOwnTasksResponse.Task> j =
                        tasks.listIterator();
                    while (j.hasNext()) {
                        TaskQueuePb.TaskQueueQueryAndOwnTasksResponse.Task task =
                            j.next();
                        if (taskName.equals(task.getTaskName())) {
                            j.remove();
                            break;
                        }
                    }
                }
                return new TaskQueuePb.TaskQueueDeleteResponse().toByteArray();
            }
            return parent.makeSyncCall(
                environment,
                packageName,
                methodName,
                request);
        }
    }
}
