package gaej2011.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import gaej2011.meta.ProfileMeta;
import gaej2011.meta.UploadUrlDTOMeta;
import gaej2011.model.Profile;
import gaej2011.model.UploadUrlDTO;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.Channels;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.ControllerTestCase;
import org.slim3.tester.TestEnvironment;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.files.FileWriteChannel;
import com.google.apphosting.api.ApiProxy;

public class ImagesControllerTest extends ControllerTestCase {

    static final String PATH = "/images";

    @Test
    public void アップロード用URLを取得する_ログインしていない() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        tester.start(PATH);
        assertThat(
            " レスポンスコードが401",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_UNAUTHORIZED));
    }

    @Test
    public void アップロード用URLを取得する() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        // test@example.com というユーザがログイン中、という状態を作っておく。
        TestEnvironment environment =
            (TestEnvironment) ApiProxy.getCurrentEnvironment();
        environment.setEmail("test@example.com");
        tester.start(PATH);
        assertThat(
            "ImagesController のインスタンスが使用される",
            tester.getController(),
            instanceOf(ImagesController.class));
        assertThat(
            " レスポンスコードが200",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_OK));
        assertThat(
            "Content-Type はapplication/json",
            tester.response.getContentType(),
            is("application/json"));
        assertThat(
            "Chancter-Encoding はutf-8",
            tester.response.getCharacterEncoding(),
            is("utf-8"));
        UploadUrlDTO dto =
            UploadUrlDTOMeta.get().jsonToModel(
                tester.response.getOutputAsString());
        assertThat("uploadUrl が返される", dto.getUploadUrl(), is(notNullValue()));
    }

    @Test
    public void リダイレクト後のjQueryUploadPlugin向けのレスポンス()
            throws NullPointerException, IllegalArgumentException, IOException,
            ServletException {
        // Blobstore にテスト用の画像を追加し、パラメータ用のBlobKey を取得する。
        File testImageFile = new File("test/appengine-silver-120x30.gif");
        byte[] imageBytes = new byte[(int) testImageFile.length()];
        DataInputStream input =
            new DataInputStream(new FileInputStream(testImageFile));
        input.readFully(imageBytes);
        input.close();
        FileService fileService = FileServiceFactory.getFileService();
        AppEngineFile file =
            fileService
                .createNewBlobFile("images/gif", testImageFile.getName());
        FileWriteChannel writeChannel =
            fileService.openWriteChannel(file, true);
        OutputStream output = Channels.newOutputStream(writeChannel);
        output.write(imageBytes);
        output.close();
        writeChannel.closeFinally();
        BlobKey blobKey = fileService.getBlobKey(file);
        // test@example.com というユーザがログイン中、という状態を作っておく。
        TestEnvironment environment =
            (TestEnvironment) ApiProxy.getCurrentEnvironment();
        environment.setEmail("test@example.com");
        int beforeCount = tester.count(Profile.class);
        tester.param("key", blobKey.getKeyString());
        tester.start(PATH);
        assertThat(
            " レスポンスコードが200",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_OK));
        assertThat(
            "jQuery Plugin のためのContent-Type はtext/html",
            tester.response.getContentType(),
            is("text/html"));
        assertThat(
            "Chancter-Encoding はutf-8",
            tester.response.getCharacterEncoding(),
            is("utf-8"));
        Profile profile =
            ProfileMeta.get().jsonToModel(tester.response.getOutputAsString());
        assertThat(
            "JSON にimageUrl のエントリが含まれる",
            profile.getProfileURL(),
            is(notNullValue()));
        int afterCount = tester.count(Profile.class);
        assertThat("Profile が一件増える", afterCount, is(beforeCount + 1));
    }

    @Test
    public void ユーザのプロフィール画像の取得_画像を設定していないユーザ() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        tester.param("user", "uso800");
        tester.start(PATH);
        assertThat(
            "ImagesController のインスタンスが使用される",
            tester.getController(),
            instanceOf(ImagesController.class));
        assertThat(
            " レスポンスコードが404",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_NOT_FOUND));
    }

    @Test
    public void ユーザのプロフィール画像の取得() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        // ユーザに対応するProfile を準備する。
        Profile profile = new Profile();
        profile.setKey(Datastore.createKey(Profile.class, "1"));
        profile.setProfileURL("dummy_url");
        Datastore.put(profile);
        tester.param("user", "1");
        tester.start(PATH);
        assertThat(
            "ImagesController のインスタンスが使用される",
            tester.getController(),
            instanceOf(ImagesController.class));
        assertThat(" リダイレクトが返される", tester.isRedirect(), is(true));
        assertThat(
            " リダイレクト先が画像へのURL",
            tester.getDestinationPath(),
            is("dummy_url"));
    }
}
