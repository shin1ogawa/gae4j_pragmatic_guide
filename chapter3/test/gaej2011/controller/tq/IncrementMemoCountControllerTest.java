package gaej2011.controller.tq;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import gaej2011.model.Minutes;
import gaej2011.service.MinutesService;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.ControllerTestCase;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.prospectivesearch.ProspectiveSearchPb;
import com.google.apphosting.api.ApiProxy;
import com.google.apphosting.api.ApiProxy.ApiConfig;
import com.google.apphosting.api.ApiProxy.ApiProxyException;
import com.google.apphosting.api.ApiProxy.Delegate;
import com.google.apphosting.api.ApiProxy.Environment;
import com.google.apphosting.api.ApiProxy.LogRecord;

public class IncrementMemoCountControllerTest extends ControllerTestCase {

    static final String PATH = "/tq/IncrementMemoCount";

    @Test
    public void タスクを実行する() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        Key minutesKey = MinutesService.put(" テスト用議事録1");
        Minutes before = Datastore.get(Minutes.class, minutesKey);
        tester.request.addParameter(
            "minutesKey",
            Datastore.keyToString(minutesKey));
        tester.start(PATH);
        assertThat(
            "IncrementMemoCountController のインスタンスが使用される",
            tester.getController(),
            instanceOf(IncrementMemoCountController.class));
        assertThat(
            " レスポンスコードが200",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_OK));
        Minutes after = Datastore.get(Minutes.class, minutesKey);
        assertThat(
            " メモ件数が1 増える",
            after.getMemoCount(),
            is(before.getMemoCount() + 1));
    }

    ProspectiveSearchDelegate delegate;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        delegate = new ProspectiveSearchDelegate();
        ApiProxy.setDelegate(delegate);
    }

    @Override
    public void tearDown() throws Exception {
        ApiProxy.setDelegate(delegate.parent);
        super.tearDown();
    }

    static class ProspectiveSearchDelegate implements Delegate<Environment> {
        @SuppressWarnings("unchecked")
        final Delegate<Environment> parent = ApiProxy.getDelegate();

        public void flushLogs(Environment environment) {
            parent.flushLogs(environment);
        }

        public List<Thread> getRequestThreads(Environment environment) {
            return parent.getRequestThreads(environment);
        }

        public void log(Environment environment, LogRecord record) {
            parent.log(environment, record);
        }

        public Future<byte[]> makeAsyncCall(Environment environment,
                String packageName, String methodName, byte[] request,
                ApiConfig apiConfig) {
            return parent.makeAsyncCall(
                environment,
                packageName,
                methodName,
                request,
                apiConfig);
        }

        boolean matchWasExecuted = false;

        public byte[] makeSyncCall(Environment environment, String packageName,
                String methodName, byte[] request) throws ApiProxyException {
            if (packageName.equals("matcher") && methodName.equals("Match")) {
                matchWasExecuted = true;
                ProspectiveSearchPb.MatchResponse responsePb =
                    new ProspectiveSearchPb.MatchResponse();
                return responsePb.toByteArray();
            } else {
                return parent.makeSyncCall(
                    environment,
                    packageName,
                    methodName,
                    request);
            }
        }
    }
}
