package gaej2011.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import gaej2011.meta.MemoMeta;
import gaej2011.model.Memo;
import gaej2011.service.MinutesService;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.ControllerTestCase;
import org.slim3.tester.TestEnvironment;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.taskqueue.TaskQueuePb.TaskQueueAddRequest;
import com.google.apphosting.api.ApiProxy;

public class MemoControllerTest extends ControllerTestCase {

    static final String PATH = "/Memo";

    @Test
    public void 特定の議事録にメモを追加できる() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        // test@example.com というユーザがログイン中、という状態を作っておく。
        TestEnvironment environment =
            (TestEnvironment) ApiProxy.getCurrentEnvironment();
        environment.setEmail("test@example.com");

        Key minutesKey = MinutesService.put(" テスト用議事録1");
        tester.request.setMethod("POST");
        tester.param("minutes", Datastore.keyToString(minutesKey));
        tester.param("memo", " テスト用議事録1");
        tester.start(PATH);
        assertThat(
            "MemoController のインスタンスが使用される",
            tester.getController(),
            instanceOf(MemoController.class));
        assertThat(
            " レスポンスコードが204",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_NO_CONTENT));
    }

    @Test
    public void 特定の議事録にメモを追加できる_memoを指定しない() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        // test@example.com というユーザがログイン中、という状態を作っておく。
        TestEnvironment environment =
            (TestEnvironment) ApiProxy.getCurrentEnvironment();
        environment.setEmail("test@example.com");

        Key minutesKey = MinutesService.put(" テスト用議事録1");
        tester.request.setMethod("POST");
        tester.param("minutes", Datastore.keyToString(minutesKey));
        tester.start(PATH);
        assertThat(
            "MemoController のインスタンスが使用される",
            tester.getController(),
            instanceOf(MemoController.class));
        assertThat(
            " レスポンスコードが400",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test
    public void 特定の議事録にメモを追加できる_minutesを指定しない() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        // test@example.com というユーザがログイン中、という状態を作っておく。
        TestEnvironment environment =
            (TestEnvironment) ApiProxy.getCurrentEnvironment();
        environment.setEmail("test@example.com");

        @SuppressWarnings("unused")
        Key minutesKey = MinutesService.put(" テスト用議事録1");
        tester.request.setMethod("POST");
        tester.param("memo", " テスト用議事録1");
        tester.start(PATH);
        assertThat(
            "MemoController のインスタンスが使用される",
            tester.getController(),
            instanceOf(MemoController.class));
        assertThat(
            " レスポンスコードが400",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test
    public void 特定の議事録にメモを追加できる_無効なminutesを指定する() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        // test@example.com というユーザがログイン中、という状態を作っておく。
        TestEnvironment environment =
            (TestEnvironment) ApiProxy.getCurrentEnvironment();
        environment.setEmail("test@example.com");

        @SuppressWarnings("unused")
        Key minutesKey = MinutesService.put(" テスト用議事録1");
        tester.request.setMethod("POST");
        tester.param("minutes", "uso800");
        tester.param("memo", " テスト用議事録1");
        tester.start(PATH);
        assertThat(
            "MemoController のインスタンスが使用される",
            tester.getController(),
            instanceOf(MemoController.class));
        assertThat(
            " レスポンスコードが400",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test
    public void 特定の議事録にメモを追加できる_ログインしていない() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        @SuppressWarnings("unused")
        Key minutesKey = MinutesService.put(" テスト用議事録1");
        tester.request.setMethod("POST");
        tester.start(PATH);
        assertThat(
            " レスポンスコードが401",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_UNAUTHORIZED));
    }

    @Test
    public void 議事録ごとに議事録に追加されたメモを古い順に一覧表示できる() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        Key minutesKey = MinutesService.put(" テスト用議事録1");
        Calendar calendar = Calendar.getInstance();
        for (int i = 0; i < 5; i++) { // 5 件保存しておく
            Memo memo = new Memo();
            memo.setMinutes(minutesKey);
            memo.setMemo(" メモ" + i);
            memo.setCreatedAt(calendar.getTime());
            Datastore.put(memo);
            calendar.add(Calendar.HOUR_OF_DAY, 1); // 1 時間進める
        }
        tester.request.setMethod("GET");
        tester.param("minutes", Datastore.keyToString(minutesKey));
        tester.start(PATH);
        assertThat(
            "MemoController のインスタンスが使用される",
            tester.getController(),
            instanceOf(MemoController.class));
        assertThat(
            " レスポンスコードが200",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_OK));
        assertThat(
            "Content-Type はapplication/json",
            tester.response.getContentType(),
            is("application/json"));
        assertThat(
            "Chancter-Encoding はutf-8",
            tester.response.getCharacterEncoding(),
            is("utf-8"));
        String json = tester.response.getOutputAsString();
        Memo[] memos = MemoMeta.get().jsonToModels(json);
        assertThat(" 全てのメモが返る", memos.length, is(5));
    }

    @Test
    public void 議事録ごとに議事録に追加されたメモを古い順に一覧表示できる_minutesを指定しない()
            throws NullPointerException, IllegalArgumentException, IOException,
            ServletException {
        tester.request.setMethod("GET");
        tester.start(PATH);
        assertThat(
            "MemoController のインスタンスが使用される",
            tester.getController(),
            instanceOf(MemoController.class));
        assertThat(
            " レスポンスコードが400",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test
    public void 議事録ごとに議事録に追加されたメモを古い順に一覧表示できる_minutesに無効な値を指定する()
            throws NullPointerException, IllegalArgumentException, IOException,
            ServletException {
        tester.request.setMethod("GET");
        tester.param("minutes", "uso800");
        tester.start(PATH);
        assertThat(
            "MemoController のインスタンスが使用される",
            tester.getController(),
            instanceOf(MemoController.class));
        assertThat(
            " レスポンスコードが400",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test
    public void GETリクエストのときにアクセスカウンタ用のタスクが追加される() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        Key minutesKey = MinutesService.put(" テスト用議事録1");
        int before = tester.tasks.size();
        tester.param("minutes", Datastore.keyToString(minutesKey));
        tester.start(PATH);
        int after = tester.tasks.size();
        assertThat("Task が一件追加される", after, is(before + 1));
        TaskQueueAddRequest task = tester.tasks.get(0);
        assertThat(
            "access-log キューにTask が追加される",
            task.getQueueName(),
            is("access-log"));
        assertThat(
            "Task にminutesKey パラメータが設定される",
            task.getBody().startsWith("minutesKey="),
            is(true));
    }
}
