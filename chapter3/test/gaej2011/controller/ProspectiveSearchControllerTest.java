package gaej2011.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import gaej2011.meta.MinutesMeta;
import gaej2011.model.Minutes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import javax.servlet.ServletException;

import org.junit.Test;
import org.slim3.tester.ControllerTestCase;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.prospectivesearch.ProspectiveSearchService;
import com.google.appengine.api.prospectivesearch.ProspectiveSearchServiceFactory;
import com.google.appengine.api.prospectivesearch.Subscription;
import com.google.appengine.api.xmpp.JID;
import com.google.appengine.api.xmpp.XMPPServiceFactory;
import com.google.appengine.api.xmpp.XMPPServicePb;
import com.google.apphosting.api.ApiProxy;
import com.google.apphosting.api.ApiProxy.ApiConfig;
import com.google.apphosting.api.ApiProxy.ApiProxyException;
import com.google.apphosting.api.ApiProxy.Delegate;
import com.google.apphosting.api.ApiProxy.Environment;
import com.google.apphosting.api.ApiProxy.LogRecord;

public class ProspectiveSearchControllerTest extends ControllerTestCase {

    static final String PATH = "/prospectiveSearch";

    @Test
    public void HotMinutesトピックにクエリが登録される() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        tester.start(PATH);
        ProspectiveSearchService service =
            ProspectiveSearchServiceFactory.getProspectiveSearchService();
        List<Subscription> subscriptions =
            service.listSubscriptions("HotMinutes");
        assertThat(" 三段階のクエリが登録される", subscriptions.size(), is(3));
        for (Subscription subscription : subscriptions) {
            assertThat("memoCount = [25|50|100]", subscription
                .getQuery()
                .matches("memoCount = (25|50|100)"), is(true));
        }
    }

    @Test
    public void XMPP招待が送信される() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        tester.start(PATH);
        assertThat(" 招待が一件送信される", delegate.invitations.size(), is(1));
        assertThat(
            " 宛先",
            delegate.invitations.get(0).getJid(),
            is(ProspectiveSearchController.ADMIN_EMAIL));
    }

    @Test
    public void XMPPメッセージが送信される() {
        Minutes minutes = new Minutes();
        minutes.setTitle(" 活発な議事録");
        minutes.setMemoCount(100);
        Entity document = MinutesMeta.get().modelToEntity(minutes);
        ProspectiveSearchController.sendMessage(document);
        assertThat(" メッセージが一件送信される", delegate.messages.size(), is(1));
        assertThat(
            " 宛先",
            delegate.messages.get(0).getJid(0),
            is(ProspectiveSearchController.ADMIN_EMAIL));
        assertThat(
            " 指定した宛先に送られる",
            delegate.messages.get(0).getBody(),
            is(" 議事録' 活発な議事録' に100 件目が投稿されました."));
        XMPPServiceFactory.getXMPPService().getPresence(
            new JID(ProspectiveSearchController.ADMIN_EMAIL));
    }

    XMPPDelegate delegate;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        delegate = new XMPPDelegate();
        ApiProxy.setDelegate(delegate);
    }

    @Override
    public void tearDown() throws Exception {
        ApiProxy.setDelegate(delegate.parent);
        super.tearDown();
    }

    static class XMPPDelegate implements Delegate<Environment> {
        @SuppressWarnings("unchecked")
        final Delegate<Environment> parent = ApiProxy.getDelegate();

        public void flushLogs(Environment environment) {
            parent.flushLogs(environment);
        }

        public List<Thread> getRequestThreads(Environment environment) {
            return parent.getRequestThreads(environment);
        }

        public void log(Environment environment, LogRecord record) {
            parent.log(environment, record);
        }

        public Future<byte[]> makeAsyncCall(Environment environment,
                String packageName, String methodName, byte[] request,
                ApiConfig apiConfig) {
            return parent.makeAsyncCall(
                environment,
                packageName,
                methodName,
                request,
                apiConfig);
        }

        final List<XMPPServicePb.XmppMessageRequest> messages =
            new ArrayList<XMPPServicePb.XmppMessageRequest>();
        final List<XMPPServicePb.XmppInviteRequest> invitations =
            new ArrayList<XMPPServicePb.XmppInviteRequest>();

        public byte[] makeSyncCall(Environment environment, String packageName,
                String methodName, byte[] request) throws ApiProxyException {
            if (packageName.equals("xmpp") && methodName.equals("SendMessage")) {
                XMPPServicePb.XmppMessageRequest requestPb =
                    new XMPPServicePb.XmppMessageRequest();
                requestPb.mergeFrom(request);
                messages.add(requestPb);
            } else if (packageName.equals("xmpp")
                && methodName.equals("SendInvite")) {
                XMPPServicePb.XmppInviteRequest requestPb =
                    new XMPPServicePb.XmppInviteRequest();
                requestPb.mergeFrom(request);
                invitations.add(requestPb);
            }
            return parent.makeSyncCall(
                environment,
                packageName,
                methodName,
                request);
        }
    }
}
