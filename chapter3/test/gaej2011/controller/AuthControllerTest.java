package gaej2011.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import gaej2011.meta.AuthDTOMeta;
import gaej2011.model.AuthDTO;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.slim3.tester.ControllerTestCase;
import org.slim3.tester.TestEnvironment;

import com.google.apphosting.api.ApiProxy;

public class AuthControllerTest extends ControllerTestCase {

    static final String PATH = "/auth";

    @Test
    public void ログインしていない() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        tester.start(PATH);
        assertThat(
            "AuthController のインスタンスが使用される",
            tester.getController(),
            instanceOf(AuthController.class));
        assertThat(
            " レスポンスコードが200",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_OK));
        String json = tester.response.getOutputAsString();
        AuthDTO dto = AuthDTOMeta.get().jsonToModel(json);
        assertThat(" ログイン中でない", dto.isLoggedIn(), is(false));
        assertThat(" ログイン用URL が返される", dto.getLoginURL(), is(notNullValue()));
        assertThat(" ログアウト用URL は返されない", dto.getLogoutURL(), is(nullValue()));
    }

    @Test
    public void ログインしている() throws NullPointerException,
            IllegalArgumentException, IOException, ServletException {
        // test@example.com というユーザがログイン中、という状態を作っておく。
        TestEnvironment environment =
            (TestEnvironment) ApiProxy.getCurrentEnvironment();
        environment.setEmail("test@example.com");
        tester.start(PATH);
        assertThat(
            "AuthController のインスタンスが使用される",
            tester.getController(),
            instanceOf(AuthController.class));
        assertThat(
            " レスポンスコードが200",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_OK));
        String json = tester.response.getOutputAsString();
        AuthDTO dto = AuthDTOMeta.get().jsonToModel(json);
        assertThat(" ログイン中", dto.isLoggedIn(), is(true));
        assertThat(" ログイン用URL は返されない", dto.getLoginURL(), is(nullValue()));
        assertThat(" ログアウト用URL が返される", dto.getLogoutURL(), is(notNullValue()));
    }
}
