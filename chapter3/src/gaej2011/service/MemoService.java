package gaej2011.service;

import gaej2011.meta.MemoMeta;
import gaej2011.model.Memo;

import java.util.Date;
import java.util.List;

import org.slim3.datastore.Datastore;
import org.slim3.memcache.Memcache;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.users.UserServiceFactory;

public class MemoService {

    public static Key put(Key minutesKey, String string) {
        Memo memo = new Memo();
        memo.setMinutes(minutesKey);
        memo.setMemo(string);
        memo.setCreatedAt(new Date());
        memo.setAuthor(UserServiceFactory.getUserService().getCurrentUser());
        Datastore.put(memo);
        Memcache.delete(minutesKey);
        Queue queue = QueueFactory.getDefaultQueue();
        queue.add(TaskOptions.Builder.withUrl("/tq/IncrementMemoCount").param(
            "minutesKey",
            Datastore.keyToString(minutesKey)));
        QueueFactory.getQueue("parse").add(
            TaskOptions.Builder.withUrl("/tq/Yahoo").param(
                "memoKey",
                Datastore.keyToString(memo.getKey())));
        return memo.getKey();
    }

    static final MemoMeta meta = MemoMeta.get();

    public static List<Memo> list(Key minutesKey) {
        List<Memo> list = Memcache.get(minutesKey);
        if (list != null) {
            return list;
        }
        list =
            Datastore
                .query(meta)
                .filter(meta.minutes.equal(minutesKey))
                .sort(meta.createdAt.asc)
                .asList();
        Memcache.put(minutesKey, list);
        return list;
    }
}
