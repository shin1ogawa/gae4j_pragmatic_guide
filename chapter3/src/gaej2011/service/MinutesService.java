package gaej2011.service;

import gaej2011.meta.MemoMeta;
import gaej2011.meta.MinutesMeta;
import gaej2011.model.Memo;
import gaej2011.model.Minutes;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.Channels;
import java.util.Date;
import java.util.List;

import org.slim3.datastore.Datastore;
import org.slim3.memcache.Memcache;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.files.FileWriteChannel;
import com.google.appengine.api.prospectivesearch.ProspectiveSearchServiceFactory;
import com.google.appengine.api.users.UserServiceFactory;

public class MinutesService {

    public static Key put(String string) {
        Minutes minutes = new Minutes();
        minutes.setTitle(string);
        minutes.setCreatedAt(new Date());
        minutes.setAuthor(UserServiceFactory.getUserService().getCurrentUser());
        Datastore.put(minutes);
        Memcache.delete(MEMCACHE_KEY_LIST);
        return minutes.getKey();
    }

    static final MinutesMeta meta = MinutesMeta.get();

    static final String MEMCACHE_KEY_LIST = "LIST_OF_MINUTES";

    public static List<Minutes> list() {
        List<Minutes> list = Memcache.get(MEMCACHE_KEY_LIST);
        if (list != null) {
            return list;
        }
        list = Datastore.query(meta).sort(meta.createdAt.desc).asList();
        Memcache.put(MEMCACHE_KEY_LIST, list);
        return list;
    }

    public static void incrementMemoCount(Key minutesKey) {
        Transaction tx = Datastore.beginTransaction();
        try {
            Minutes minutes = Datastore.get(tx, meta, minutesKey);
            minutes.setMemoCount(minutes.getMemoCount() + 1);
            minutes.setUpdatedAt(new Date());
            ProspectiveSearchServiceFactory
                .getProspectiveSearchService()
                .match(meta.modelToEntity(minutes), "HotMinutes");
            Datastore.put(tx, minutes);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
        Memcache.delete(MEMCACHE_KEY_LIST);
    }

    public static List<Key> queryForUpdateMemoCount() {
        long now = new Date().getTime();
        Date before24hours = new Date(now - 24 * 60 * 60 * 1000);
        Date before48hours = new Date(now - 48 * 60 * 60 * 1000);
        return Datastore
            .query(meta)
            .filter(meta.updatedAt.lessThan(before24hours))
            .filter(meta.updatedAt.greaterThan(before48hours))
            .asKeyList();
    }

    public static void updateMemoCount(Key minutesKey) {
        MemoMeta memoMeta = MemoMeta.get();
        int memoCount =
            Datastore
                .query(memoMeta)
                .filter(memoMeta.minutes.equal(minutesKey))
                .count();
        Minutes minutes = Datastore.get(meta, minutesKey);
        minutes.setMemoCount(memoCount);
        Datastore.put(minutes);
        Memcache.delete(MEMCACHE_KEY_LIST);
    }

    public static void deleteMinutes(Minutes minutes) {
        Key minutesKey = minutes.getKey();
        Memcache.delete(minutesKey);
        Memcache.delete(MEMCACHE_KEY_LIST);
        List<Key> deleteKeyList =
            Datastore
                .query(Memo.class)
                .filter(MemoMeta.get().minutes.equal(minutesKey))
                .asKeyList();
        Datastore.delete(deleteKeyList);
        Datastore.delete(minutesKey);
    }

    public static BlobKey exportAsTSV(Minutes minutes) throws IOException {
        FileService fileService = FileServiceFactory.getFileService();
        AppEngineFile file =
            fileService.createNewBlobFile(
                "text/tab-separated-values",
                minutes.getTitle());
        FileWriteChannel writeChannel =
            fileService.openWriteChannel(file, true);
        PrintWriter writer =
            new PrintWriter(Channels.newWriter(writeChannel, "utf-8"));
        List<Memo> list = MemoService.list(minutes.getKey());
        for (Memo memo : list) {
            writer.print("\"");
            writer.print(memo.getCreatedAt());
            writer.print("\"");
            writer.print("\t");
            writer.print("\"");
            writer.print(memo.getAuthor().getEmail());
            writer.print("\"");
            writer.print("\t");
            writer.print("\"");
            writer.print(memo.getMemo());
            writer.print("\"");
            writer.println();
        }
        writer.close();
        writeChannel.closeFinally();
        return fileService.getBlobKey(file);
    }
}
