package gaej2011.model;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Model;

import com.google.appengine.api.datastore.Key;

@Model
public class AccessCounter {
    @Attribute(primaryKey = true)
    Key key;
    
    String minutesTitle;
    long count = 0;

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public String getMinutesTitle() {
        return minutesTitle;
    }

    public void setMinutesTitle(String minutesTitle) {
        this.minutesTitle = minutesTitle;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
