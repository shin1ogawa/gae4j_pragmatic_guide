package gaej2011.model;

import java.io.Serializable;
import java.util.Date;
import org.slim3.datastore.Attribute;
import org.slim3.datastore.Model;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.users.User;

@Model
public class Memo implements Serializable {
    private static final long serialVersionUID = 6828696835969661688L;

    @Attribute(primaryKey = true)
    Key key;
    Key minutes;
    String memo;
    Date createdAt;
    User author;

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public Key getMinutes() {
        return minutes;
    }

    public void setMinutes(Key minutes) {
        this.minutes = minutes;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}