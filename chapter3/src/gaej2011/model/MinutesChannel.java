package gaej2011.model;

import java.util.Date;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Model;
import org.slim3.datastore.json.Json;

import com.google.appengine.api.datastore.Key;

@Model
public class MinutesChannel {
    @Attribute(primaryKey = true)
    @Json(ignore = true)
    Key key; // keyName はクライアントID

    @Attribute(persistent = false)
    String token;

    Key minutesKey;
    Date createdAt;

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Key getMinutesKey() {
        return minutesKey;
    }

    public void setMinutesKey(Key minutesKey) {
        this.minutesKey = minutesKey;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
