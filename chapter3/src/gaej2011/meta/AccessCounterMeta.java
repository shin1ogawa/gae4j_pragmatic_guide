package gaej2011.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2012-02-13 22:26:47")
/** */
public final class AccessCounterMeta extends org.slim3.datastore.ModelMeta<gaej2011.model.AccessCounter> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.AccessCounter, java.lang.Long> count = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.AccessCounter, java.lang.Long>(this, "count", "count", long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.AccessCounter, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.AccessCounter, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<gaej2011.model.AccessCounter> minutesTitle = new org.slim3.datastore.StringAttributeMeta<gaej2011.model.AccessCounter>(this, "minutesTitle", "minutesTitle");

    private static final AccessCounterMeta slim3_singleton = new AccessCounterMeta();

    /**
     * @return the singleton
     */
    public static AccessCounterMeta get() {
       return slim3_singleton;
    }

    /** */
    public AccessCounterMeta() {
        super("AccessCounter", gaej2011.model.AccessCounter.class);
    }

    @Override
    public gaej2011.model.AccessCounter entityToModel(com.google.appengine.api.datastore.Entity entity) {
        gaej2011.model.AccessCounter model = new gaej2011.model.AccessCounter();
        model.setCount(longToPrimitiveLong((java.lang.Long) entity.getProperty("count")));
        model.setKey(entity.getKey());
        model.setMinutesTitle((java.lang.String) entity.getProperty("minutesTitle"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        gaej2011.model.AccessCounter m = (gaej2011.model.AccessCounter) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("count", m.getCount());
        entity.setProperty("minutesTitle", m.getMinutesTitle());
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        gaej2011.model.AccessCounter m = (gaej2011.model.AccessCounter) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        gaej2011.model.AccessCounter m = (gaej2011.model.AccessCounter) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        throw new IllegalStateException("The version property of the model(gaej2011.model.AccessCounter) is not defined.");
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        gaej2011.model.AccessCounter m = (gaej2011.model.AccessCounter) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        writer.setNextPropertyName("count");
        encoder0.encode(writer, m.getCount());
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getMinutesTitle() != null){
            writer.setNextPropertyName("minutesTitle");
            encoder0.encode(writer, m.getMinutesTitle());
        }
        writer.endObject();
    }

    @Override
    protected gaej2011.model.AccessCounter jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        gaej2011.model.AccessCounter m = new gaej2011.model.AccessCounter();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("count");
        m.setCount(decoder0.decode(reader, m.getCount()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("minutesTitle");
        m.setMinutesTitle(decoder0.decode(reader, m.getMinutesTitle()));
        return m;
    }
}