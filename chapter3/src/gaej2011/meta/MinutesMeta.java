package gaej2011.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2012-02-13 17:38:10")
/** */
public final class MinutesMeta extends org.slim3.datastore.ModelMeta<gaej2011.model.Minutes> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Minutes, com.google.appengine.api.users.User> author = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Minutes, com.google.appengine.api.users.User>(this, "author", "author", com.google.appengine.api.users.User.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Minutes, java.util.Date> createdAt = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Minutes, java.util.Date>(this, "createdAt", "createdAt", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Minutes, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Minutes, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Minutes, java.lang.Integer> memoCount = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Minutes, java.lang.Integer>(this, "memoCount", "memoCount", int.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<gaej2011.model.Minutes> title = new org.slim3.datastore.StringAttributeMeta<gaej2011.model.Minutes>(this, "title", "title");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Minutes, java.util.Date> updatedAt = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Minutes, java.util.Date>(this, "updatedAt", "updatedAt", java.util.Date.class);

    private static final MinutesMeta slim3_singleton = new MinutesMeta();

    /**
     * @return the singleton
     */
    public static MinutesMeta get() {
       return slim3_singleton;
    }

    /** */
    public MinutesMeta() {
        super("Minutes", gaej2011.model.Minutes.class);
    }

    @Override
    public gaej2011.model.Minutes entityToModel(com.google.appengine.api.datastore.Entity entity) {
        gaej2011.model.Minutes model = new gaej2011.model.Minutes();
        model.setAuthor((com.google.appengine.api.users.User) entity.getProperty("author"));
        model.setCreatedAt((java.util.Date) entity.getProperty("createdAt"));
        model.setKey(entity.getKey());
        model.setMemoCount(longToPrimitiveInt((java.lang.Long) entity.getProperty("memoCount")));
        model.setTitle((java.lang.String) entity.getProperty("title"));
        model.setUpdatedAt((java.util.Date) entity.getProperty("updatedAt"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        gaej2011.model.Minutes m = (gaej2011.model.Minutes) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("author", m.getAuthor());
        entity.setProperty("createdAt", m.getCreatedAt());
        entity.setProperty("memoCount", m.getMemoCount());
        entity.setProperty("title", m.getTitle());
        entity.setProperty("updatedAt", m.getUpdatedAt());
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        gaej2011.model.Minutes m = (gaej2011.model.Minutes) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        gaej2011.model.Minutes m = (gaej2011.model.Minutes) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        throw new IllegalStateException("The version property of the model(gaej2011.model.Minutes) is not defined.");
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        gaej2011.model.Minutes m = (gaej2011.model.Minutes) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getAuthor() != null){
            writer.setNextPropertyName("author");
            encoder0.encode(writer, m.getAuthor());
        }
        if(m.getCreatedAt() != null){
            writer.setNextPropertyName("createdAt");
            encoder0.encode(writer, m.getCreatedAt());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        writer.setNextPropertyName("memoCount");
        encoder0.encode(writer, m.getMemoCount());
        if(m.getTitle() != null){
            writer.setNextPropertyName("title");
            encoder0.encode(writer, m.getTitle());
        }
        if(m.getUpdatedAt() != null){
            writer.setNextPropertyName("updatedAt");
            encoder0.encode(writer, m.getUpdatedAt());
        }
        writer.endObject();
    }

    @Override
    protected gaej2011.model.Minutes jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        gaej2011.model.Minutes m = new gaej2011.model.Minutes();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("author");
        m.setAuthor(decoder0.decode(reader, m.getAuthor()));
        reader = rootReader.newObjectReader("createdAt");
        m.setCreatedAt(decoder0.decode(reader, m.getCreatedAt()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("memoCount");
        m.setMemoCount(decoder0.decode(reader, m.getMemoCount()));
        reader = rootReader.newObjectReader("title");
        m.setTitle(decoder0.decode(reader, m.getTitle()));
        reader = rootReader.newObjectReader("updatedAt");
        m.setUpdatedAt(decoder0.decode(reader, m.getUpdatedAt()));
        return m;
    }
}