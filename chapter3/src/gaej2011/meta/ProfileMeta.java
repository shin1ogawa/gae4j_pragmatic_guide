package gaej2011.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2012-02-13 20:30:36")
/** */
public final class ProfileMeta extends org.slim3.datastore.ModelMeta<gaej2011.model.Profile> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Profile, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Profile, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<gaej2011.model.Profile> profileURL = new org.slim3.datastore.StringAttributeMeta<gaej2011.model.Profile>(this, "profileURL", "profileURL");

    private static final ProfileMeta slim3_singleton = new ProfileMeta();

    /**
     * @return the singleton
     */
    public static ProfileMeta get() {
       return slim3_singleton;
    }

    /** */
    public ProfileMeta() {
        super("Profile", gaej2011.model.Profile.class);
    }

    @Override
    public gaej2011.model.Profile entityToModel(com.google.appengine.api.datastore.Entity entity) {
        gaej2011.model.Profile model = new gaej2011.model.Profile();
        model.setKey(entity.getKey());
        model.setProfileURL((java.lang.String) entity.getProperty("profileURL"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        gaej2011.model.Profile m = (gaej2011.model.Profile) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("profileURL", m.getProfileURL());
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        gaej2011.model.Profile m = (gaej2011.model.Profile) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        gaej2011.model.Profile m = (gaej2011.model.Profile) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        throw new IllegalStateException("The version property of the model(gaej2011.model.Profile) is not defined.");
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        gaej2011.model.Profile m = (gaej2011.model.Profile) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getProfileURL() != null){
            writer.setNextPropertyName("profileURL");
            encoder0.encode(writer, m.getProfileURL());
        }
        writer.endObject();
    }

    @Override
    protected gaej2011.model.Profile jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        gaej2011.model.Profile m = new gaej2011.model.Profile();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("profileURL");
        m.setProfileURL(decoder0.decode(reader, m.getProfileURL()));
        return m;
    }
}