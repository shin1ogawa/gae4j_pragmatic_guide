package gaej2011.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2012-02-13 18:05:03")
/** */
public final class MinutesChannelMeta extends org.slim3.datastore.ModelMeta<gaej2011.model.MinutesChannel> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.MinutesChannel, java.util.Date> createdAt = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.MinutesChannel, java.util.Date>(this, "createdAt", "createdAt", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.MinutesChannel, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.MinutesChannel, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.MinutesChannel, com.google.appengine.api.datastore.Key> minutesKey = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.MinutesChannel, com.google.appengine.api.datastore.Key>(this, "minutesKey", "minutesKey", com.google.appengine.api.datastore.Key.class);

    private static final MinutesChannelMeta slim3_singleton = new MinutesChannelMeta();

    /**
     * @return the singleton
     */
    public static MinutesChannelMeta get() {
       return slim3_singleton;
    }

    /** */
    public MinutesChannelMeta() {
        super("MinutesChannel", gaej2011.model.MinutesChannel.class);
    }

    @Override
    public gaej2011.model.MinutesChannel entityToModel(com.google.appengine.api.datastore.Entity entity) {
        gaej2011.model.MinutesChannel model = new gaej2011.model.MinutesChannel();
        model.setCreatedAt((java.util.Date) entity.getProperty("createdAt"));
        model.setKey(entity.getKey());
        model.setMinutesKey((com.google.appengine.api.datastore.Key) entity.getProperty("minutesKey"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        gaej2011.model.MinutesChannel m = (gaej2011.model.MinutesChannel) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("createdAt", m.getCreatedAt());
        entity.setProperty("minutesKey", m.getMinutesKey());
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        gaej2011.model.MinutesChannel m = (gaej2011.model.MinutesChannel) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        gaej2011.model.MinutesChannel m = (gaej2011.model.MinutesChannel) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        throw new IllegalStateException("The version property of the model(gaej2011.model.MinutesChannel) is not defined.");
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        gaej2011.model.MinutesChannel m = (gaej2011.model.MinutesChannel) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getCreatedAt() != null){
            writer.setNextPropertyName("createdAt");
            encoder0.encode(writer, m.getCreatedAt());
        }
        if(m.getMinutesKey() != null){
            writer.setNextPropertyName("minutesKey");
            encoder0.encode(writer, m.getMinutesKey());
        }
        if(m.getToken() != null){
            writer.setNextPropertyName("token");
            encoder0.encode(writer, m.getToken());
        }
        writer.endObject();
    }

    @Override
    protected gaej2011.model.MinutesChannel jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        gaej2011.model.MinutesChannel m = new gaej2011.model.MinutesChannel();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("createdAt");
        m.setCreatedAt(decoder0.decode(reader, m.getCreatedAt()));
        reader = rootReader.newObjectReader("minutesKey");
        m.setMinutesKey(decoder0.decode(reader, m.getMinutesKey()));
        reader = rootReader.newObjectReader("token");
        m.setToken(decoder0.decode(reader, m.getToken()));
        return m;
    }
}