package gaej2011.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2012-02-13 15:19:02")
/** */
public final class MemoMeta extends org.slim3.datastore.ModelMeta<gaej2011.model.Memo> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Memo, com.google.appengine.api.users.User> author = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Memo, com.google.appengine.api.users.User>(this, "author", "author", com.google.appengine.api.users.User.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Memo, java.util.Date> createdAt = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Memo, java.util.Date>(this, "createdAt", "createdAt", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Memo, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Memo, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<gaej2011.model.Memo> memo = new org.slim3.datastore.StringAttributeMeta<gaej2011.model.Memo>(this, "memo", "memo");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Memo, com.google.appengine.api.datastore.Key> minutes = new org.slim3.datastore.CoreAttributeMeta<gaej2011.model.Memo, com.google.appengine.api.datastore.Key>(this, "minutes", "minutes", com.google.appengine.api.datastore.Key.class);

    private static final MemoMeta slim3_singleton = new MemoMeta();

    /**
     * @return the singleton
     */
    public static MemoMeta get() {
       return slim3_singleton;
    }

    /** */
    public MemoMeta() {
        super("Memo", gaej2011.model.Memo.class);
    }

    @Override
    public gaej2011.model.Memo entityToModel(com.google.appengine.api.datastore.Entity entity) {
        gaej2011.model.Memo model = new gaej2011.model.Memo();
        model.setAuthor((com.google.appengine.api.users.User) entity.getProperty("author"));
        model.setCreatedAt((java.util.Date) entity.getProperty("createdAt"));
        model.setKey(entity.getKey());
        model.setMemo((java.lang.String) entity.getProperty("memo"));
        model.setMinutes((com.google.appengine.api.datastore.Key) entity.getProperty("minutes"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        gaej2011.model.Memo m = (gaej2011.model.Memo) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("author", m.getAuthor());
        entity.setProperty("createdAt", m.getCreatedAt());
        entity.setProperty("memo", m.getMemo());
        entity.setProperty("minutes", m.getMinutes());
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        gaej2011.model.Memo m = (gaej2011.model.Memo) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        gaej2011.model.Memo m = (gaej2011.model.Memo) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        throw new IllegalStateException("The version property of the model(gaej2011.model.Memo) is not defined.");
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        gaej2011.model.Memo m = (gaej2011.model.Memo) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getAuthor() != null){
            writer.setNextPropertyName("author");
            encoder0.encode(writer, m.getAuthor());
        }
        if(m.getCreatedAt() != null){
            writer.setNextPropertyName("createdAt");
            encoder0.encode(writer, m.getCreatedAt());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getMemo() != null){
            writer.setNextPropertyName("memo");
            encoder0.encode(writer, m.getMemo());
        }
        if(m.getMinutes() != null){
            writer.setNextPropertyName("minutes");
            encoder0.encode(writer, m.getMinutes());
        }
        writer.endObject();
    }

    @Override
    protected gaej2011.model.Memo jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        gaej2011.model.Memo m = new gaej2011.model.Memo();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("author");
        m.setAuthor(decoder0.decode(reader, m.getAuthor()));
        reader = rootReader.newObjectReader("createdAt");
        m.setCreatedAt(decoder0.decode(reader, m.getCreatedAt()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("memo");
        m.setMemo(decoder0.decode(reader, m.getMemo()));
        reader = rootReader.newObjectReader("minutes");
        m.setMinutes(decoder0.decode(reader, m.getMinutes()));
        return m;
    }
}