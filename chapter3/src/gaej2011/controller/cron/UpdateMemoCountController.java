package gaej2011.controller.cron;

import gaej2011.service.MinutesService;

import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import com.google.appengine.api.datastore.Key;

public class UpdateMemoCountController extends Controller {

    @Override
    protected Navigation run() throws Exception {
        List<Key> list = MinutesService.queryForUpdateMemoCount();
        for (Key minutesKey : list) {
            MinutesService.updateMemoCount(minutesKey);
        }
        return null;
    }
}
