package gaej2011.controller;

import gaej2011.meta.KeyWordMeta;
import gaej2011.meta.MemoMeta;
import gaej2011.model.KeyWord;
import gaej2011.model.Memo;
import gaej2011.service.YahooAPIService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.ModelQuery;

import com.google.appengine.api.datastore.Key;

public class SearchController extends Controller {

    @Override
    protected Navigation run() throws Exception {
        String query = asString("query");
        Set<String> keyWords = YahooAPIService.parse(query);
        KeyWordMeta keyWordMeta = KeyWordMeta.get();
        // 検索対象となるのはKeyWord エンティティ
        ModelQuery<KeyWord> modelQuery = Datastore.query(keyWordMeta);
        for (String keyWord : keyWords) {
            // すべてのキーワードをEQ フィルタとして追加し、すべてのキーワードにマッチする、というクエリを組み立てる
            modelQuery.filter(keyWordMeta.words.equal(keyWord));
        }
        // クエリを実行し、KeyWord エンティティのキーのリストを取得する
        List<Key> keyList = modelQuery.asKeyList();
        // KeyWord エンティティのキーのリストからMemo エンティティのキーを取得する。
        Set<Key> memoKeyList = new HashSet<Key>(keyList.size());
        for (Key key : keyList) {
            memoKeyList.add(key.getParent());
        }
        // Memo エンティティのキーのリストからMemo エンティティを取得する。
        List<Memo> memos = Datastore.get(Memo.class, memoKeyList);
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().write(MemoMeta.get().modelsToJson(memos));
        response.flushBuffer();
        return null;
    }
}
