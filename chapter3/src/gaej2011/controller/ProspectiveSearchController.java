package gaej2011.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.prospectivesearch.FieldType;
import com.google.appengine.api.prospectivesearch.ProspectiveSearchService;
import com.google.appengine.api.prospectivesearch.ProspectiveSearchServiceFactory;
import com.google.appengine.api.xmpp.JID;
import com.google.appengine.api.xmpp.Message;
import com.google.appengine.api.xmpp.MessageBuilder;
import com.google.appengine.api.xmpp.XMPPServiceFactory;

public class ProspectiveSearchController extends Controller {
    static final Logger logger = Logger
        .getLogger(ProspectiveSearchController.class.getName());

    static final String ADMIN_EMAIL = "admin@example.com";

    @Override
    protected Navigation run() throws Exception {
        if (isPost()) {
            return doPost();
        } else {
            return doGet();
        }
    }

    Navigation doPost() {
        ProspectiveSearchService prospectiveSearchService =
            ProspectiveSearchServiceFactory.getProspectiveSearchService();
        Entity document = prospectiveSearchService.getDocument(request);
        logger.info("Topic:" + asString("topic") + ", id=" + asString("id"));
        logger.info(document.toString());
        sendMessage(document);
        return null;
    }

    Navigation doGet() {
        ProspectiveSearchService prospectiveSearchService =
            ProspectiveSearchServiceFactory.getProspectiveSearchService();
        String topic = "HotMinutes";
        long leaseDurationSeconds = 0;
        Map<String, FieldType> schema = new HashMap<String, FieldType>();
        schema.put("memoCount", FieldType.INT32);
        prospectiveSearchService.subscribe(
            topic,
            "HotMinutes_25",
            leaseDurationSeconds,
            "memoCount = 25",
            schema);
        prospectiveSearchService.subscribe(
            topic,
            "HotMinutes_50",
            leaseDurationSeconds,
            "memoCount = 50",
            schema);
        prospectiveSearchService.subscribe(
            topic,
            "HotMinutes_100",
            leaseDurationSeconds,
            "memoCount = 100",
            schema);
        XMPPServiceFactory
            .getXMPPService()
            .sendInvitation(new JID(ADMIN_EMAIL));
        return null;
    }

    public static void sendMessage(Entity document) {
        StringBuilder b = new StringBuilder();
        b.append(" 議事録'");
        b.append(document.getProperty("title"));
        b.append("' に");
        b.append(document.getProperty("memoCount"));
        b.append(" 件目が投稿されました.");
        Message message =
            new MessageBuilder()
                .withRecipientJids(new JID(ADMIN_EMAIL))
                .withBody(b.toString())
                .build();
        XMPPServiceFactory.getXMPPService().sendMessage(message);
    }
}
