package gaej2011.controller.tq;

import gaej2011.meta.MemoMeta;
import gaej2011.model.KeyWord;
import gaej2011.model.Memo;
import gaej2011.service.YahooAPIService;

import java.util.Set;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;

public class YahooController extends Controller {

    @Override
    protected Navigation run() throws Exception {
        Key memoKey = asKey("memoKey");
        Memo memo = Datastore.get(MemoMeta.get(), memoKey);
        Set<String> keywords = YahooAPIService.parse(memo.getMemo());
        KeyWord keyWord = new KeyWord();
        keyWord.setKey(Datastore.createKey(memoKey, KeyWord.class, "1"));
        keyWord.setWords(keywords);
        Datastore.put(keyWord);
        return null;
    }
}
