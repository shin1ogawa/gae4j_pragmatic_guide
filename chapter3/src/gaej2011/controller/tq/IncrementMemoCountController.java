package gaej2011.controller.tq;

import gaej2011.service.MinutesService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

public class IncrementMemoCountController extends Controller {

    @Override
    protected Navigation run() throws Exception {
        MinutesService.incrementMemoCount(asKey("minutesKey"));
        return null;
    }
}
