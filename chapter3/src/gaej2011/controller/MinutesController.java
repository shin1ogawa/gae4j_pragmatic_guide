package gaej2011.controller;

import gaej2011.meta.MinutesMeta;
import gaej2011.model.Minutes;
import gaej2011.service.MinutesService;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;
import org.slim3.util.StringUtil;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.mail.MailService;
import com.google.appengine.api.mail.MailService.Message;
import com.google.appengine.api.mail.MailServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;

public class MinutesController extends Controller {

    @Override
    protected Navigation run() throws Exception {
        if (isPost()) {
            return doPost();
        } else if (StringUtil.isEmpty(asString("download")) == false) {
            return doDownload();
        } else if (StringUtil.isEmpty(asString("delete")) == false) {
            return doDelete();
        } else {
            return doGet();
        }
    }

    private Navigation doDelete() throws IOException {
        User currentUser = UserServiceFactory.getUserService().getCurrentUser();
        if (currentUser == null) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }
        Key minutesKey;
        try {
            minutesKey = asKey("delete");
            if (minutesKey == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return null;
            }
        } catch (IllegalArgumentException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        Minutes minutes = Datastore.getOrNull(Minutes.class, minutesKey);
        if (minutes == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        if (minutes.getAuthor().equals(currentUser) == false) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
        // TSV 作成と削除をする
        BlobKey blobKey = MinutesService.exportAsTSV(minutes);
        MinutesService.deleteMinutes(minutes);
        // ダウンロードURL をメールで送信する
        Message message = new Message();
        message.setSender("minutes@yourappid.appspotmail.com");
        message.setSubject(" 議事録[" + minutes.getTitle() + "] がTSV に変換されました");
        message.setTo(currentUser.getEmail());
        StringBuilder b = new StringBuilder();
        b
            .append(request.getScheme())
            .append("://")
            .append(request.getServerName());
        if (request.getServerPort() != 80) {
            b.append(":").append(request.getServerPort());
        }
        b.append("/minutes?download=").append(blobKey.getKeyString());
        message.setTextBody(b.toString());
        MailService mailService = MailServiceFactory.getMailService();
        mailService.send(message);
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        return null;
    }

    private Navigation doDownload() throws IOException {
        String blobKeyString = asString("download");
        if (StringUtil.isEmpty(blobKeyString)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        BlobstoreService blobstoreService =
            BlobstoreServiceFactory.getBlobstoreService();
        BlobKey blobKey = new BlobKey(blobKeyString);
        BlobInfo blobInfo = new BlobInfoFactory().loadBlobInfo(blobKey);
        response.setContentType(blobInfo.getContentType());
        response.setContentLength((int) blobInfo.getSize());
        response.setHeader(
            "Content-disposition",
            "attachment;" + blobInfo.getFilename());
        byte[] data =
            blobstoreService.fetchData(blobKey, 0, blobInfo.getSize());
        response.getOutputStream().write(data);
        response.flushBuffer();
        return null;
    }

    Navigation doGet() throws IOException {
        List<Minutes> minutes = MinutesService.list();
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().write(MinutesMeta.get().modelsToJson(minutes));
        response.flushBuffer();
        return null;
    }

    Navigation doPost() throws IOException {
        if (UserServiceFactory.getUserService().getCurrentUser() == null) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }
        String title = asString("title");
        if (StringUtil.isEmpty(title)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        Key key = MinutesService.put(title);
        sendMail(KeyFactory.keyToString(key));
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        return null;
    }

    private void sendMail(String keyString) throws IOException {
        Message message = new Message();
        message.setSender("minutes@yourappid.appspotmail.com");
        message.setSubject(" 新しい議事録が追加されました");
        StringBuilder b = new StringBuilder();
        b
            .append(request.getScheme())
            .append("://")
            .append(request.getServerName());
        if (request.getServerPort() != 80) {
            b.append(":").append(request.getServerPort());
        }
        b.append("/minutes.html?minutes=").append(keyString);
        message.setTextBody(b.toString());
        MailService mailService = MailServiceFactory.getMailService();
        mailService.sendToAdmins(message);
    }
}
