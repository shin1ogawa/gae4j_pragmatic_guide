package gaej2011.controller;

import gaej2011.meta.AuthDTOMeta;
import gaej2011.model.AuthDTO;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class AuthController extends Controller {

    @Override
    protected Navigation run() throws Exception {
        UserService userService = UserServiceFactory.getUserService();
        AuthDTO authDTO = new AuthDTO();
        if (userService.isUserLoggedIn()) {
            authDTO.setLoggedIn(true);
            authDTO.setLogoutURL(userService.createLogoutURL("/"));
        } else {
            authDTO.setLoggedIn(false);
            authDTO.setLoginURL(userService.createLoginURL("/"));
        }
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().println(AuthDTOMeta.get().modelToJson(authDTO));
        response.flushBuffer();
        return null;
    }
}
