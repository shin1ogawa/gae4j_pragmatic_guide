package gaej2011.controller;

import gaej2011.meta.MemoMeta;
import gaej2011.model.Memo;
import gaej2011.service.MemoService;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;
import org.slim3.util.StringUtil;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.users.UserServiceFactory;

public class MemoController extends Controller {

    @Override
    protected Navigation run() throws Exception {
        if (isPost()) {
            return doPost();
        } else {
            return doGet();
        }
    }

    private Navigation doGet() throws IOException {
        Key minutesKey;
        try {
            minutesKey = asKey("minutes");
            if (minutesKey == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return null;
            }
        } catch (IllegalArgumentException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        List<Memo> list = MemoService.list(minutesKey);
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().write(MemoMeta.get().modelsToJson(list));
        response.flushBuffer();
        QueueFactory.getQueue("access-log").add(
            TaskOptions.Builder.withMethod(TaskOptions.Method.PULL).param(
                "minutesKey",
                Datastore.keyToString(minutesKey)));
        return null;
    }

    Navigation doPost() {
        if (UserServiceFactory.getUserService().getCurrentUser() == null) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }
        String memo = asString("memo");
        String minutesString = asString("minutes");
        if (StringUtil.isEmpty(memo)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        if (StringUtil.isEmpty(minutesString)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        Key minutesKey;
        try {
            minutesKey = Datastore.stringToKey(minutesString);
        } catch (IllegalArgumentException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        Key memoKey = MemoService.put(minutesKey, memo);
        ChannelController.pushMemo(memoKey); // 投稿をチャンネルにプッシュする
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        return null;
    }

}
