package gaej2011;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.AppEngineTestCase;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;

public class GuestbookServiceTest extends AppEngineTestCase {

    @Test
    public void saveToDatastore() {
        int before = tester.count("Guestbook");
        GuestbookService.saveToDatastore(" 投稿内容");
        int after = tester.count("Guestbook");
        assertThat("Guestbook エンティティが1 件増えている", after, is(before + 1));
    }

    @Test
    public void saveToDatastore_entityの内容() {
        Key key = GuestbookService.saveToDatastore(" 投稿内容");
        Entity entity = Datastore.get(key);
        assertThat(
            " 投稿内容が保存されている",
            (String) entity.getProperty("message"),
            is(" 投稿内容"));
        assertThat(
            " 保存日時が保存されている",
            entity.getProperty("createdAt"),
            is(notNullValue()));
    }

    @Test
    public void queryFromDatastore() {
        Calendar calendar = Calendar.getInstance();
        for (int i = 0; i < 5; i++) { // 5 件保存しておく
            Entity entity = new Entity("Guestbook");
            entity.setProperty("message", " 投稿内容" + i);
            entity.setProperty("createdAt", calendar.getTime());
            Datastore.put(entity);
            calendar.add(Calendar.HOUR_OF_DAY, 1); // 1 時間進める
        }
        int count = 0;
        Date before = null;
        Iterable<Entity> iterable = GuestbookService.queryFromDatastore();
        for (Entity entity : iterable) {
            Date createdAt = (Date) entity.getProperty("createdAt");
            if (before != null) {
                assertThat(
                    " 新しいものから取得できている",
                    before.compareTo(createdAt) > 0,
                    is(true));
            }
            before = createdAt;
            count++;
        }
        assertThat(" 全てのエンティティが取得できている", count, is(5));
    }
}
