package gaej2011.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.*;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.ControllerTestCase;

import com.google.appengine.api.datastore.Entity;

public class GuestbookControllerTest extends ControllerTestCase {

    @Test
    public void get() throws NullPointerException, IllegalArgumentException,
            IOException, ServletException {
        tester.request.setMethod("GET");
        tester.start("/guestbook");
        assertThat(
            "200 が返る",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_OK));
        assertThat(
            "GuestbookController のインスタンスが使用される",
            tester.getController(),
            instanceOf(GuestbookController.class));
        assertThat(
            " コンテンツが空でない",
            tester.response.getOutputAsString(),
            is(notNullValue()));
        assertThat(
            "html が返る",
            tester.response.getOutputAsString(),
            containsString("<html"));
    }

    @Test
    public void post() throws NullPointerException, IllegalArgumentException,
            IOException, ServletException {
        tester.request.setMethod("POST");
        tester.param("message", " 投稿内容");
        tester.start("/guestbook");
        assertThat(
            "GuestbookController のインスタンスが使用される",
            tester.getController(),
            instanceOf(GuestbookController.class));
        assertThat(" リダイレクトを要求する", tester.isRedirect(), is(true));
        assertThat(
            " リダイレクト先が/guestbook である",
            tester.getDestinationPath(),
            is("/guestbook"));
    }

    @Test
    public void list() throws NullPointerException, IllegalArgumentException,
            IOException, ServletException {
        Calendar calendar = Calendar.getInstance();
        for (int i = 0; i < 5; i++) { // 5 件保存しておく
            Entity entity = new Entity("Guestbook");
            entity.setProperty("message", " 投稿内容" + i);
            entity.setProperty("createdAt", calendar.getTime());
            Datastore.put(entity);
            calendar.add(Calendar.HOUR_OF_DAY, 1); // 1 時間進める
        }
        tester.request.setMethod("GET");
        tester.start("/guestbook");
        assertThat(
            "200 が返る",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_OK));
        assertThat(
            "GuestbookController のインスタンスが使用される",
            tester.getController(),
            instanceOf(GuestbookController.class));
        assertThat(
            " コンテンツが空でない",
            tester.response.getOutputAsString(),
            is(notNullValue()));
        assertThat(
            "li タグが含まれている",
            tester.response.getOutputAsString(),
            containsString("<li"));
    }
}
