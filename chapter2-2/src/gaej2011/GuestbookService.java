package gaej2011;

import java.util.Date;

import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query.SortDirection;

public class GuestbookService {

    public static Key saveToDatastore(String string) {
        Entity entity = new Entity("Guestbook");
        entity.setProperty("message", string);
        entity.setProperty("createdAt", new Date());
        Datastore.put(entity);
        return entity.getKey();
    }

    public static Iterable<Entity> queryFromDatastore() {
        return Datastore
            .query("Guestbook")
            .sort("createdAt", SortDirection.DESCENDING)
            .asIterable();
    }
}
