package gaej2011.controller;

import gaej2011.GuestbookService;

import java.io.IOException;
import java.io.PrintWriter;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import com.google.appengine.api.datastore.Entity;

public class GuestbookController extends Controller {
    @Override
    protected Navigation run() throws Exception {
        if (isGet()) {
            return doGet();
        } else {
            return doPost();
        }
    }

    Navigation doPost() {
        String message = asString("message");
        GuestbookService.saveToDatastore(message);
        return redirect("/guestbook");
    }

    Navigation doGet() throws IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        PrintWriter writer = response.getWriter();
        writer.println("<html><head><title> ゲストブック</title></head>");
        writer.println("<body><h1> ゲストブック</h1>");
        writer.println("<p> 何か書いてね</p>");
        writer.println("<div><form action='/guestbook' method='post'>");
        writer.println("<p><input type='text' name='message' size='50'/>");
        writer.println("<input type='submit' /></p>");
        writer.println("</form></div>");
        Iterable<Entity> list = GuestbookService.queryFromDatastore();
        writer.println("<div><ul>");
        for (Entity entity : list) {
            writer.println("<li>"
                + entity.getProperty("message")
                + "("
                + entity.getProperty("createdAt")
                + ")</li>");
        }
        writer.println("</ul></div>");
        writer.println("</body></html>");
        response.flushBuffer();
        return null;
    }
}