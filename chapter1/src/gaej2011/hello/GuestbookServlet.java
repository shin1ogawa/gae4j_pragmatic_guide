package gaej2011.hello;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.SortDirection;

@SuppressWarnings("serial")
public class GuestbookServlet extends HttpServlet {
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String message = req.getParameter("message");
    System.out.println("message=" + message);
    Entity entity = new Entity("Guestbook");
    entity.setProperty("message", message);
    entity.setProperty("createdAt", new Date());
    DatastoreService datastoreService = DatastoreServiceFactory
        .getDatastoreService();
    datastoreService.put(entity);
    resp.sendRedirect("/guestbook");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    resp.setContentType("text/html");
    resp.setCharacterEncoding("utf-8");
    PrintWriter writer = resp.getWriter();
    writer.println("<html><head><title> ゲストブック</title></head>");
    writer.println("<body><h1> ゲストブック</h1>");
    writer.println("<p> 何か書いてね</p>");
    writer.println("<div><form action='/guestbook' method='post'>");
    writer.println("<p><input type='text' name='message' size='50'/>");
    writer.println("<input type='submit' /></p>");
    writer.println("</form></div>");
    // メッセージを日付の降順で取得する
    Query query = new Query("Guestbook");
    query.addSort("createdAt", SortDirection.DESCENDING);
    DatastoreService datastoreService = DatastoreServiceFactory
        .getDatastoreService();
    Iterable<Entity> list = datastoreService.prepare(query).asIterable();
    writer.println("<div><ul>");
    for (Entity entity : list) {
      writer.println("<li>" + entity.getProperty("message") + "("
          + entity.getProperty("createdAt") + ")</li>");
    }
    writer.println("</ul></div>");
    writer.println("</body></html>");
    resp.flushBuffer();
  }
}
