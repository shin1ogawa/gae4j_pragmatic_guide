package gaej2011.service;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import gaej2011.model.Guestbook;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.AppEngineTestCase;

import com.google.appengine.api.datastore.Key;

public class GuestbookServiceTest extends AppEngineTestCase {
    @Test
    public void saveToDatastore() {
        int before = tester.count("Guestbook");
        GuestbookService.saveToDatastore(" 投稿内容");
        int after = tester.count("Guestbook");
        assertThat("Guestbook エンティティが1 件増えている", after, is(before + 1));
    }

    @Test
    public void saveToDatastore_entityの内容() {
        Key key = GuestbookService.saveToDatastore(" 投稿内容");
        Guestbook guestbook = Datastore.get(Guestbook.class, key);
        assertThat(" 投稿内容が保存されている", guestbook.getMessage(), is(" 投稿内容"));
        assertThat(
            " 保存日時が保存されている",
            guestbook.getCreatedAt(),
            is(notNullValue()));
    }

    @Test
    public void queryFromDatastore() {
        Calendar calendar = Calendar.getInstance();
        for (int i = 0; i < 5; i++) { // 5 件保存しておく
            Guestbook guestbook = new Guestbook();
            guestbook.setMessage(" 投稿内容" + i);
            guestbook.setCreatedAt(calendar.getTime());
            Datastore.put(guestbook);
            calendar.add(Calendar.HOUR_OF_DAY, 1); // 1 時間進める
        }
        int count = 0;
        Date before = null;
        List<Guestbook> iterable = GuestbookService.queryFromDatastore();
        for (Guestbook entity : iterable) {
            Date createdAt = entity.getCreatedAt();
            if (before != null) {
                assertThat(
                    " 新しいものから取得できている",
                    before.compareTo(createdAt) > 0,
                    is(true));
            }
            before = createdAt;
            count++;
        }
        assertThat(" 全てのエンティティが取得できている", count, is(5));
    }
}