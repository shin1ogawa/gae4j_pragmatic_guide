package gaej2011.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import gaej2011.meta.GuestbookMeta;
import gaej2011.model.Guestbook;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.ControllerTestCase;

public class GuestbookControllerTest extends ControllerTestCase {

    @Test
    public void post() throws NullPointerException, IllegalArgumentException,
            IOException, ServletException {
        tester.request.setMethod("POST");
        tester.param("message", " 投稿内容");
        tester.start("/guestbook");
        assertThat(
            "GuestbookController のインスタンスが使用される",
            tester.getController(),
            instanceOf(GuestbookController.class));
        assertThat(
            " レスポンスコードが204",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_NO_CONTENT));
    }

    @Test
    public void get() throws NullPointerException, IllegalArgumentException,
            IOException, ServletException {
        Calendar calendar = Calendar.getInstance();
        for (int i = 0; i < 5; i++) { // 5 件保存しておく
            Guestbook guestbook = new Guestbook();
            guestbook.setMessage(" 投稿内容" + i);
            guestbook.setCreatedAt(calendar.getTime());
            Datastore.put(guestbook);
            calendar.add(Calendar.HOUR_OF_DAY, 1); // 1 時間進める
        }
        tester.request.setMethod("GET");
        tester.start("/guestbook");
        assertThat(
            "GuestbookController のインスタンスが使用される",
            tester.getController(),
            instanceOf(GuestbookController.class));
        assertThat(
            " レスポンスコードが200",
            tester.response.getStatus(),
            is(HttpServletResponse.SC_OK));
        assertThat(
            "Content-Type はapplication/json",
            tester.response.getContentType(),
            is("application/json"));
        assertThat(
            "Chancter-Encoding はapplication/json",
            tester.response.getCharacterEncoding(),
            is("utf-8"));
        String outputAsString = tester.response.getOutputAsString();
        Guestbook[] models = GuestbookMeta.get().jsonToModels(outputAsString);
        assertThat("Guestbook エンティティの件数と同じ", models.length, is(5));
    }
}
