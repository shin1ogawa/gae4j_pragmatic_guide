package gaej2011.controller;

import gaej2011.meta.GuestbookMeta;
import gaej2011.model.Guestbook;
import gaej2011.service.GuestbookService;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

public class GuestbookController extends Controller {

    static final Logger logger = Logger.getLogger(GuestbookController.class
        .getName());

    @Override
    protected Navigation run() throws Exception {
        if (isPost()) {
            return doPost();
        } else {
            return doGet();
        }
    }

    static final GuestbookMeta meta = GuestbookMeta.get();

    Navigation doGet() throws IOException {
        List<Guestbook> list = GuestbookService.queryFromDatastore();
        String json = meta.modelsToJson(list);
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().println(json);
        return null;
    }

    Navigation doPost() {
        String message = asString("message");
        logger.info("message=" + message);
        GuestbookService.saveToDatastore(message);
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        return null;
    }
}
