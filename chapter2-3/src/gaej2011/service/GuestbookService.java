package gaej2011.service;

import gaej2011.meta.GuestbookMeta;
import gaej2011.model.Guestbook;

import java.util.Date;
import java.util.List;

import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;

public class GuestbookService {

    public static Key saveToDatastore(String string) {
        Guestbook guestbook = new Guestbook();
        guestbook.setMessage(string);
        guestbook.setCreatedAt(new Date());
        Datastore.put(guestbook);
        return guestbook.getKey();
    }

    static final GuestbookMeta meta = GuestbookMeta.get();

    public static List<Guestbook> queryFromDatastore() {
        return Datastore.query(meta).sort(meta.createdAt.desc).asList();
    }
}
